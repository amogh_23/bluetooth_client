import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Layouts 1.3

Window {
    id: window
    // @disable-check M16
    width: Screen.width
    // @disable-check M16
    height: Screen.height
    // @disable-check M16
    visible: true
    // @disable-check M16
    title: qsTr("Bluetooth Client")

    Connections {
        target: bTClientObj
        function onMessageReceived(sender,message){
            senderName.text = sender;
            receivedData.text = message;
        }
    }

    RowLayout{
        id: firstRowLayout
        spacing: 2
        y: 52
        Text {
            id: senderNameLabel
            anchors.left: firstRowLayout.leftMargin
            //            Layout.maximumHeight: 60
            //            Layout.maximumWidth: 300
            text: qsTr("Receiving Message From : ")
            font.pixelSize: 18
            font.styleName: "Bold"
            minimumPixelSize: 12
        }
        Text {
            id: senderName
            anchors.right: firstRowLayout.rightMargin
            Layout.maximumHeight: 60
            Layout.maximumWidth: 210
            text: qsTr("loading sender . . .")
            font.pixelSize: 18
            font.styleName: "Bold"
            minimumPixelSize: 12
        }
    }

    ColumnLayout{
        id: firstColumnLayout
        spacing: 2
        y: 152
        x: Screen.width/2 - 120

        Rectangle {
            Layout.alignment: Qt.AlignCenter
//            color: "red"
            Layout.preferredWidth: 200
            Layout.preferredHeight: 40
            Text {
                id: info1
                text: qsTr("Received Data :  ")
                font.pixelSize: 20
                font.styleName: "Bold"
                minimumPixelSize: 12
            }
        }
        Rectangle {
            Layout.alignment: Qt.AlignCenter
//            color: "red"
            Layout.preferredWidth: 200
            Layout.preferredHeight: 40
            Text {
                id: receivedData
                text: qsTr("Receiving Data. . . ")
                font.pixelSize: 20
                font.styleName: "Bold"
                minimumPixelSize: 12
            }
        }
    }
}

/*##^##
Designer {
    D{i:0;autoSize:true;formeditorZoom:0.9;height:480;width:640}
}
##^##*/
