#include "bluetooth_client.h"

bluetooth_client::bluetooth_client(QObject *parent) : QObject(parent)
{

}

bluetooth_client::~bluetooth_client()
{
    stopClient();
}

void bluetooth_client::startClient(const QBluetoothServiceInfo &remoteService)
{
    if (socket) return;

    // Connect to service
    socket = new QBluetoothSocket(QBluetoothServiceInfo::RfcommProtocol);
    qDebug() << "Create socket";
    socket->connectToService(remoteService);
    qDebug() << "ConnectToService done";

    connect(socket, &QBluetoothSocket::readyRead, this, &bluetooth_client::readSocket);
    connect(socket, &QBluetoothSocket::connected, this, QOverload<>::of(&bluetooth_client::connected));
    connect(socket, &QBluetoothSocket::disconnected, this, &bluetooth_client::disconnected);
    connect(socket, QOverload<QBluetoothSocket::SocketError>::of(&QBluetoothSocket::error),
            this, &bluetooth_client::onSocketErrorOccurred);
}

void bluetooth_client::stopClient()
{
    delete socket;
    socket = nullptr;
    delete m_discoveryAgent;
}

void bluetooth_client::sendMessage(const QString &message)
{
    QByteArray text = message.toUtf8() + '\n';
    socket->write(text);
}

void bluetooth_client::readSocket()
{
    if (!socket)    return;
    while (socket->canReadLine()) {
        QByteArray line = socket->readLine();
        emit messageReceived(socket->peerName(), QString::fromUtf8(line.constData(), line.length()));
    }
}

void bluetooth_client::connected()
{
    qDebug() << "Connected to : " << socket->peerName();
    //    emit connected(socket->peerName());
}

void bluetooth_client::disconnected()
{
    qDebug() << "Disconnected from server";
}

void bluetooth_client::onSocketErrorOccurred(QBluetoothSocket::SocketError error)
{
    if (error == QBluetoothSocket::NoSocketError)   return;

    QMetaEnum metaEnum = QMetaEnum::fromType<QBluetoothSocket::SocketError>();
    QString errorString = socket->peerName() + QLatin1Char(' ')
            + metaEnum.valueToKey(error) + QLatin1String(" occurred");

    emit socketErrorOccurred(errorString);
}

void bluetooth_client::searchAndConnectToService()
{
    const QBluetoothAddress localAdapterAddress = localAdapter.address();

    m_discoveryAgent = new QBluetoothServiceDiscoveryAgent(localAdapterAddress);
    connect(m_discoveryAgent, SIGNAL(serviceDiscovered(QBluetoothServiceInfo)),
            this, SLOT(serviceDiscovered(QBluetoothServiceInfo)));
    connect(m_discoveryAgent, SIGNAL(finished()), this, SLOT(discoveryFinished()));
    connect(m_discoveryAgent, SIGNAL(canceled()), this, SLOT(discoveryFinished()));
    connect(m_discoveryAgent, SIGNAL(error(QBluetoothServiceDiscoveryAgent::Error)),
            this, SLOT(serviceDiscoveryError(QBluetoothServiceDiscoveryAgent::Error)));

    qDebug() << "Android SDK version is " << QtAndroid::androidSdkVersion();
    if (QtAndroid::androidSdkVersion() >= 23){
        m_discoveryAgent->setUuidFilter(QBluetoothUuid(reverseUuid));
    } else {
        m_discoveryAgent->setUuidFilter(QBluetoothUuid(serviceUuid));
    }

    if (m_discoveryAgent->isActive())   m_discoveryAgent->stop();
    m_discoveryAgent->start(QBluetoothServiceDiscoveryAgent::FullDiscovery);
}

void bluetooth_client::serviceDiscovered(const QBluetoothServiceInfo &serviceInfo)
{
    qDebug() << "Discovered service on" << serviceInfo.device().name() << serviceInfo.device().address().toString();
    qDebug() << "Service name:" << serviceInfo.serviceName();
    qDebug() << "Description:" << serviceInfo.attribute(QBluetoothServiceInfo::ServiceDescription).toString();
    qDebug() << "Provider:" << serviceInfo.attribute(QBluetoothServiceInfo::ServiceProvider).toString();
    qDebug() << "L2CAP protocol service multiplexer:" << serviceInfo.protocolServiceMultiplexer();
    qDebug() << "RFCOMM server channel:" << serviceInfo.serverChannel();

//    const QBluetoothAddress address = serviceInfo.device().address();
//    for (const QBluetoothServiceInfo &info : qAsConst(m_discoveredServices)) {
//        if (info.device().address() == address)
//            return;
//    }
//    QString remoteDevice;
//    if (serviceInfo.device().name().isEmpty())  remoteDevice = address.toString();
//    else    remoteDevice = serviceInfo.device().name();

//    QStringList *serviceList = new QStringList(QString::fromLatin1("%1%2").arg(remoteDevice,serviceInfo.serviceName()));
//    //connect to discovered service
//    m_discoveredServices.insert(serviceList,serviceInfo);
//    for(int i=0; i<serviceList->size(); i++){
//        qDebug() << serviceList[i];
//    }
    startClient(serviceInfo);
}

void bluetooth_client::discoveryFinished()
{
    qDebug() << "Service Discovery finished";
    QList<QBluetoothServiceInfo> serviceList = m_discoveryAgent->discoveredServices();
    if(serviceList.isEmpty()){
        qDebug() << "No ACS bluetooth service found";
        return;
    }
}

void bluetooth_client::stopServiceDiscovery()
{
    if (m_discoveryAgent->isActive())   m_discoveryAgent->stop();
}

void bluetooth_client::serviceDiscoveryError(QBluetoothServiceDiscoveryAgent::Error error)
{
    qDebug() << error;
}
