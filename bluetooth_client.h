#ifndef BLUETOOTH_CLIENT_H
#define BLUETOOTH_CLIENT_H

#include <QObject>
#include <QBluetoothServiceInfo>
#include <QBluetoothSocket>
#include <QBluetoothHostInfo>
#include <QBluetoothServiceDiscoveryAgent>
#include <QMetaEnum>
#include <QtAndroidExtras/QtAndroid>

static const QLatin1String serviceUuid("e8e10f95-1a70-4b27-9ccf-02010264e9c8");
static const QLatin1String reverseUuid("c8e96402-0102-cf9c-274b-701a950fe1e8");

class bluetooth_client : public QObject
{
    Q_OBJECT
public:
    explicit bluetooth_client(QObject *parent = nullptr);
    ~bluetooth_client();
    void startClient(const QBluetoothServiceInfo &remoteService);
    void stopClient();
    void searchAndConnectToService();

public slots:
    void sendMessage(const QString &message);

signals:
    void messageReceived(const QString &sender, const QString &message);
//    void connected(const QString &name);
    void socketErrorOccurred(const QString &errorString);

private slots:
    void readSocket();
    void connected();
    void disconnected();
    void onSocketErrorOccurred(QBluetoothSocket::SocketError);

    void serviceDiscovered(const QBluetoothServiceInfo &serviceInfo);
    void discoveryFinished();
    void stopServiceDiscovery();
    void serviceDiscoveryError(QBluetoothServiceDiscoveryAgent::Error error);

private:
    QBluetoothSocket *socket = nullptr;
    QBluetoothHostInfo localAdapter;
    QBluetoothServiceDiscoveryAgent *m_discoveryAgent;
    QMap<QStringList *, QBluetoothServiceInfo> m_discoveredServices;
};

#endif // BLUETOOTH_CLIENT_H
